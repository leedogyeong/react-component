import React, {ButtonHTMLAttributes, DetailedHTMLProps, ForwardedRef, useMemo} from 'react'
import styled, { css } from 'styled-components'



// 프로젝트 환경에 따라 달라질값
export const sizeSets = {
  S: {
    fontSize: '0.5rem',
    width: '25%',
    height: '50px',
  },
  M: {
    fontSize: '0.875rem',
    width: '45%',
    height: '50px',
  },
  L: {
    fontSize: '1rem',
    width: '100%',
    height: '50px',
  },
}

export const IconSizeSets = {
  S: {
    width: '25%',
    height: '25%',
  },
  M: {
    width: '45%',
    height: '45%',
  },
  L: {
    width: '100%',
    height: '100%',
  },
}

const defaultStyle = css`
  border: none;
  background-color: #1B1464;
  color: #fff;
  :hover {
    background-color: #4437C7;
  }
  :active {
    background-color: #180C93;
  }
`

const outlineStyle = css`
  background-color: #fff;
  border-color: #1B1464;
  border-width: 1px;
  border-style: solid;
  color: #1B1464;
  :hover {
    background-color: #4437C7;
  }
  :active {
    background-color: #180C93;
  }
`

const ghostStyle = css`
  background-color: #fff;
  border: 1px solid #DDDDDD;
  color: #333;
  :hover {
    background-color: #4437C7;
  }
  :active {
    background-color: #180C93;
  }
`

// 프로젝트 환경에 따라 달라질값
export const styles = [
 {name: 'defaultStyle', style: defaultStyle},
 {name: 'outlineStyle', style: outlineStyle},
 {name: 'ghostStyle', style: ghostStyle},
]


export type buttonSize = 'S' | 'M' | 'L'


export interface ButtonColorScheme {
  background: string
  hover: string
  active: string
  text: string
}

type ButtonVariant = 'defaultStyle' | 'outlineStyle' | 'ghostStyle'

interface ButtonProps
  extends Omit<
    DetailedHTMLProps<
      ButtonHTMLAttributes<HTMLButtonElement>,
      HTMLButtonElement
    >,
    'type'
  > {
  /**
   *  정의된 버튼 사이즈
   */
  buttonSize?: buttonSize
  /**
   *  버튼 안에 들어올 컨텐츠
   */
  children?: React.ReactNode
  /**
   * 'default' | 'outline' | 'ghost' 기본값은 'default'
   */
  variantType?: ButtonVariant
  /**
   *  버튼 html type
   */
  htmlType?: 'button' | 'reset' | 'submit'
  /**
   *  button disabled
   */
  disabled?: boolean
  /**
   * 버튼을 링크 버튼으로 사용하기 위한 값
   * href 값 입력  target 값이용 필수
   */
  href?: string
  /**
   * a 태그의 target 값
   */
  target?: React.HTMLAttributeAnchorTarget
  /**
  * ref
  */
  ref?: ForwardedRef<HTMLButtonElement>
  /**
  *  버튼 radius
  */
  radius?: number
  /**
  *  아이콘 버튼으로 사용
  */
  isIcon?: boolean
  /**
  *  사용할 아이콘
  */
  iconImg?: any
}


const ButtonCommon = (
  {
    className,
    htmlType = 'button',
    children,
    variantType = 'defaultStyle',
    buttonSize = 'L',
    href,
    target,
    isIcon,
    iconImg,
    ...rest
  }: ButtonProps,
  ref: any
) => {

  // 선택한 모양에 따라 스타일 주입
  const isVariantType = useMemo(() => {
    for (const st of styles) {
      if (st.name === variantType) {
        return st.style.join('')
      }
    }
  }, [variantType])

  const Contents = useMemo(() => {
    if(href) {
      return (
         <ADiv
         buttonStyle={isVariantType}
         buttonSize={buttonSize}

         {...rest}
         >
            <ALink aStyle={isVariantType} href={href} target={target}>
              {children}
            </ALink>
         </ADiv>
      )
    }
    if (isIcon) {
      return(
      <IconButton
        ref={ref}
        type={htmlType}
        buttonStyle={isVariantType}
        buttonSize={buttonSize}
        {...rest}
      >
        <Icon src={iconImg} alt='아이콘' />
      </IconButton>
    )}
    else {
      return (
        <Button
        ref={ref}
        type={htmlType}
        buttonStyle={isVariantType}
        buttonSize={buttonSize}
        {...rest}
        >
          {children}
        </Button>
      )
    }
  }, [href, isIcon, isVariantType, buttonSize, target, rest, children, ref, htmlType, iconImg])

  return (
    <>{Contents}</>
  )
}

export default React.forwardRef(ButtonCommon)


interface buttonUiProps extends ButtonProps {
  buttonStyle?: any

}

const commonStyle = css<buttonUiProps>`
 ${(props) =>
    css`
      ${props.buttonStyle};
    `
  }
  outline: none;
  display: inline-flex;
  justify-content: center;
  align-items: center;

  cursor: pointer;
  font-weight: 400;
  font-family: inherit;

  &:disabled {
    filter: grayscale(15%);
    opacity: 0.6;
    background-color: #eee;
    color: #8c8c8c;
    border: none;
  }

  transition: 0.1s background ease-in, 0.1s color ease-in;

  &:focus-visible {
    box-shadow: 0 0 0.5rem rgba(0, 0, 0, 0.5);
  }
`

const ButtonStyles = css<buttonUiProps>`

  // 버튼 사이즈
  width: ${(props) => props.buttonSize ? sizeSets[props.buttonSize].width : '100px'};
  height: ${(props) => props.buttonSize ? sizeSets[props.buttonSize].height : 'auto'};
  font-size: ${(props) => props.buttonSize ? sizeSets[props.buttonSize].fontSize : '1rem'};
  border-radius: ${(props) => props.radius}px;
`

const Button = styled.button<buttonUiProps>`
  ${ButtonStyles};
  ${commonStyle};
`

const ADiv = styled.button`
  ${ButtonStyles};
  ${commonStyle};
`

const ALink = styled.a<{aStyle: any}>`
 ${(props) =>
    css`
      ${props.aStyle};
    `
  }
  border: none;
  text-decoration: none;
  background-color: transparent;
  :hover {
    background-color: transparent;
  }
  :active {
    background-color: transparent;
  }

`

const IconButton  = styled.button<buttonUiProps>`
  width: ${(props) => props.buttonSize ? IconSizeSets[props.buttonSize].width : '64px'};
  height: ${(props) => props.buttonSize ? IconSizeSets[props.buttonSize].height : '64px'};
  border-radius: ${(props) => props.radius}px;
  ${commonStyle};
`

const Icon = styled.img`
  width: 24px;
  height: 24px;
`
