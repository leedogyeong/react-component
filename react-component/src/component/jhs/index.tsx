import React, {useRef} from 'react'
import styled from 'styled-components'
import ButtonCommon from './ButtonCommon';
import Img from './sliders.png'
import Img2 from './sliders_1.png'

function JhsComponent() {

  const ref = useRef<HTMLButtonElement>(null)


  return (
    <Section>
      <Wrapper>
        <Box>
        <Space />
         <L>
            <ButtonCommon
              ref={ref}
              buttonSize={'L'}
              children={'L 버튼'}
            />
         </L>
         <Space />
         <M>
            <ButtonCommon
              ref={ref}
              buttonSize={'M'}
              children={'M 버튼'}
            />
         </M>
         <Space />
         <S>
            <ButtonCommon
              ref={ref}
              buttonSize={'S'}
              children={'S 버튼'}
            />
         </S>
        </Box>

        <Box>
        <Space />
         <L>
            <ButtonCommon
              ref={ref}
              variantType="outlineStyle"
              buttonSize={'L'}
              children={'L 버튼'}
            />
         </L>
         <Space />
         <M>
            <ButtonCommon
              ref={ref}
              variantType="outlineStyle"
              buttonSize={'M'}
              children={'M 버튼'}
            />
         </M>
         <Space />
         <S>
            <ButtonCommon
              ref={ref}
              variantType="outlineStyle"
              buttonSize={'S'}
              children={'S 버튼'}
            />
         </S>
        </Box>


        <Box>
        <Space />
         <L>
            <ButtonCommon
              ref={ref}
              variantType="ghostStyle"
              buttonSize={'L'}
              children={'L 버튼'}
            />
         </L>
         <Space />
         <M>
            <ButtonCommon
              ref={ref}
              variantType="ghostStyle"
              buttonSize={'M'}
              children={'M 버튼'}
            />
         </M>
         <Space />
         <S>
            <ButtonCommon
              ref={ref}
              variantType="ghostStyle"
              buttonSize={'S'}
              children={'S 버튼'}
            />
         </S>
        </Box>

        <Box>
        <Space />
         <L>
            <ButtonCommon
              ref={ref}
              disabled
              htmlType="button"
              buttonSize={'L'}
              children={'L 버튼'}
            />
         </L>
         <Space />
         <M>
            <ButtonCommon
              ref={ref}
              disabled
              htmlType="button"
              buttonSize={'M'}
              children={'M 버튼'}
            />
         </M>
         <Space />
         <S>
            <ButtonCommon
              ref={ref}
              disabled
              htmlType="button"
              buttonSize={'S'}
              children={'S 버튼'}
            />
         </S>
        </Box>
      </Wrapper>

      <Space />
      <Wrapper>

      <Box>
        <Space />
         <L>
            <ButtonCommon
              ref={ref}
              radius={10}
              buttonSize={'L'}
              target={'_blank'}
              href="https://www.naver.com/"
              children={'L 링크 버튼'}
            />
         </L>
         <Space />
         <M>
            <ButtonCommon
              ref={ref}
              radius={10}
              variantType="outlineStyle"
              buttonSize={'M'}
              href="https://www.naver.com/"
              target='_blank'
              children={'M 링크 버튼'}
            />
         </M>
         <Space />
         <M>
            <ButtonCommon
              ref={ref}
              disabled
              radius={10}
              variantType="outlineStyle"
              buttonSize={'M'}
              href="https://www.naver.com/"
              target={'_blank'}
              children={'M 링크 버튼'}
            />
         </M>
         <Space />
         <S>
            <ButtonCommon
              ref={ref}
              radius={10}
              variantType="ghostStyle"
              href="https://www.naver.com/"
              buttonSize={'S'}
              target={'_blank'}
              children={'S 링크 버튼'}
            />
         </S>
        </Box>

        <Box>
        <Space />
         <L>
            <ButtonCommon
              isIcon
              iconImg={Img}
            />
         </L>
         <Space />
         <M>
            <ButtonCommon
              variantType="outlineStyle"
              isIcon
              iconImg={Img2}
            />
         </M>
         <Space />
         <M>
            <ButtonCommon
              variantType="ghostStyle"
              isIcon
              iconImg={Img2}
            />
         </M>
         <Space />
         <S>
            <ButtonCommon
              isIcon
              disabled
              iconImg={Img}
            />
         </S>
        </Box>
      </Wrapper>
    </Section>
  )
}

export default JhsComponent;

const Section = styled.div`
  width: calc(100vw - 20px);
  height: calc(100vh - 20px);
  align-items: center;
  display: flex;
  flex-direction: column;
  margin: 0 auto;

`

const Wrapper = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-start;
`

const Space = styled.div`
  margin-bottom: 20px;
`

const Box = styled.div`
  width: 20%;
  border: 1px solid #ccc;
  padding: 10px;
`

const L = styled.div`
`

const M = styled.div`
  display: flex;
  justify-content: space-around;
`

const S = styled.div`
  display: flex;
  justify-content: center;
  justify-content: space-around;
`
