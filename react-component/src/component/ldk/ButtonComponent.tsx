import React, {ButtonHTMLAttributes} from 'react'
import styled from 'styled-components'

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  // 버튼 컴포넌트에 추가로 필요한 prop이 있다면 여기에 추가합니다.
}

function ButtonComponent(props: ButtonProps) {
  return (
    <ButtonWrapper>
      <Button>
        <ButtonContents>
          {props.children}
        </ButtonContents>
      </Button>
    </ButtonWrapper>
  )
}

export default ButtonComponent;

const ButtonWrapper = styled.div`
  min-height: 25px;
  min-width: 62px;
`

const ButtonContents = styled.div`
  width: calc(100% - 40px);
  height: calc(100% - 20px);
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
`

const Button = styled.button<ButtonProps>`
  width: 100%;
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  border: none;
  //border: solid black 1px;
  background: #1B1464;
  color: #FFFFFF;
  :hover {
    background: #4437C7;
  };
  :active {
    background: #180C93;
  };
  :disabled {
    background: #EEEEEE;
    color: #8C8C8C;
  }
`