import React from 'react'
import { BrowserRouter, Routes, Route, NavLink } from "react-router-dom";
import LdkComponent from './component/ldk'
import JhsComponent from './component/jhs'

function App() {
  return (
    <BrowserRouter>
    <nav>
      <NavLink className={({ isActive }) => "nav-link" + (isActive ? " click" : "")} to='/jhs'>
      JHS
      </NavLink>
      <NavLink className={({ isActive }) => "nav-link" + (isActive ? " click" : "")} to='/ldk'>
      LDK
      </NavLink>
    </nav>
    <Routes>
      <Route path='/' element={<App />} />
      <Route path='/ldk' element={<LdkComponent />} />
      <Route path='/jhs' element={<JhsComponent />} />
    </Routes>
  </BrowserRouter>
  )
}

export default App;