import React from 'react';
import logo from './logo.svg';
import './App.css';
import Router from './router';

function App() {
  return (
    <body>
      <Router />
    </body>
  );
}

export default App;
